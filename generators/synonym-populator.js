var _ = require('lodash');
var Promise = require('bluebird');

function run(pipeline, settings) {
    return Promise
        .resolve()
        .then(_populator(pipeline, settings))
        .then(function(concepts) {
            pipeline.setKey('concepts', concepts);
            return concepts;
        });
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}

function _populator(pipeline, settings) {
    return function() {
        return Promise.mapSeries(pipeline.getKey('concepts'), function(o) {
            if(o.synonyms && o.synonyms.length > 0) {
                let populated = [];
                _.forEach(o.synonyms, (synonym, i, coll) => {
                    let x = _.find(pipeline.getKey('concepts'), { slug: synonym });
                    if(x) {
                        populated.push({
                            slug: synonym,
                            language: x.language,
                            name: `${x.term} (${x.source})`
                        });
                    }
                });
                o.synonyms = populated;
            }
            return o;
        });
    }
}

module.exports.step = step;
module.exports.run = run;
