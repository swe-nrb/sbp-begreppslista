var _ = require('lodash');
var Promise = require('bluebird');

function run(pipeline, settings) {
    settings.filename = settings.filename || 'collection-decorated';

    return Promise.resolve()
        .then(_readFiles(pipeline, settings))
        .spread(_mergeTags(pipeline, settings))
        .spread(_decorateConcepts(pipeline, settings));
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}
function _readFiles(pipeline, settings) {
    return () => {
        return [
            _.filter(pipeline.getKey('validObjects'), { _schema: 'http://schemas.nationella-riktlinjer.se/nrb-tag-definition.1.schema.json' }),
            _.filter(pipeline.getKey('validObjects'), { _schema: 'http://schemas.nationella-riktlinjer.se/nrb-tag.1.schema.json' })
        ];
    }
}

function _mergeTags(pipeline) {
    return (definitions, tags) => {
        pipeline.addLog(`Collected ${definitions.length} tag definitions with in total ${tags.length} tags`);
        // write tags to file
        pipeline.writeFile({
            path: `${pipeline.getOutputDirPath()}/tags.json`,
            data: JSON.stringify(definitions, null, 2),
            title: 'Taggar',
            description: 'En lista med alla taggar'
        });
        let hashTable = {};
        let finalTags = [];
        // hash em
        _.map(definitions, (definition) => {
            hashTable[definition.slug] = definition; 
        });
        _.map(tags, tag => {
            if(hashTable[tag.tag_definition]) {
                var tagObj = new Tag(hashTable[tag.tag_definition]);
                tagObj.addTagData(tag);
                // list
                finalTags.push(tagObj.output());
            } else {
                pipeline.addLog(`Tag for ${tag.concept} could not resolve definition: ${tag.tag_definition}`);
            }
        });
        return [hashTable, finalTags];
    }
}

function _decorateConcepts(pipeline, settings) {
    return function(hashTable, tags) {
        pipeline.addLog(`Got ${tags.length} tags to decorate`);
        // get concepts
        var concepts = pipeline.getKey('concepts');
        pipeline.addLog(`Decorating ${concepts.length} concepts`);
        let decoratedCount = 0;
        return Promise
            .resolve(concepts)
            .then(function() {
                return [Promise.mapSeries(concepts, (concept) => {
                    let t = _.remove(tags, { concept: concept.slug });
                    if(t.length > 0) {
                        decoratedCount = decoratedCount + t.length;
                        concept.tags = t;
                    } else {
                        concept.tags = [{
                            short_name: 'För information',
                            full_name: 'För information',
                            description: 'Sidoinformation av intresse',
                            status: 'complete',
                            percentDone: 1
                        }];
                    }
                    return concept;
                }), tags];
            })
            .spread(function(concepts, leftoverTags) {
                pipeline.addLog(`Decorated ${decoratedCount} tags to concepts`);
                if(leftoverTags.length > 0) {
                    pipeline.addLog(`${leftoverTags.length} tags could not be decorated`);
                    _.forEach(leftoverTags, (tag) => {
                        pipeline.addLog(`${tag.tag}: ${tag.concept}`);
                    });
                }
                // all issues ran
                pipeline.setKey('concepts', concepts);
                return concepts;
            });
        }
    }

    /**
     * This helper class merges the tag definition with the tag
     */
    class Tag {
        constructor(tagDefinition) {
            /**
             * Tag definition data
             */
            this.tagDefinition = tagDefinition;
            /**
             * Tag instance data
             */
            this.tagData;
            /**
             * review steps as an arrayarray from tag definition
             * @type array
             */
            this.reviewSteps = tagDefinition.review_steps;
            /**
             * Hash of review steps based on name
             * @type object
             */
            this.reviewStepsHash = {};
            this.stepProgressHash = {};
            this.reviewLength = this.reviewSteps.length;

            // store review steps for quick access
            _.forEach(this.tagDefinition.review_steps, (step) => {
                this.reviewStepsHash[step.name] = _.cloneDeep(step);
            });
        }
        /**
         * Adds the tag instance data
         * @param object data tag data object directly from tag file
         */
        addTagData(data) {
            this.tagData = data;
            /**
             * Ads the review steps stated in the tag file as a hashmap mapped to name
             */
            _.forEach(this.tagData.review_steps, (step) => {
                this.stepProgressHash[step.name] = _.cloneDeep(step);
            });
        }
        output() {
            /**
             * Output object
             */
            var outputObj = {
                tag: this.tagData.tag_definition,
                concept: this.tagData.concept,
                short_name: this.tagDefinition.short_name,
                full_name: this.tagDefinition.full_name,
                description: this.tagDefinition.description,
                review_steps: [],
                review_length: this.reviewLength,
                status: 'idle'
            };
            /**
             * Loop over review step and check progress
             */
            _.forEach(this.reviewSteps, (mainStep) => {
                let step = {
                    name: this.reviewStepsHash[mainStep.name].name,
                    description: this.reviewStepsHash[mainStep.name].description,
                    complete: false
                };
                // check if step is in progress
                if(this.stepProgressHash[mainStep.name]) {
                    // transfer comments
                    if(this.stepProgressHash[mainStep.name].comment) {
                        step.comment = this.stepProgressHash[mainStep.name].comment;
                    }
                    // transfer author
                    if(this.stepProgressHash[mainStep.name].author) {
                        step.author = this.stepProgressHash[mainStep.name].author;
                    }
                    // transfer status
                    if(this.stepProgressHash[mainStep.name].date) {
                        step.date = this.stepProgressHash[mainStep.name].date;
                    }

                    // check if step is complete
                    if(step.author && step.date) {
                        step.complete = true;
                    }
                }
                outputObj.review_steps.push(step);
            });
            // find first "non complete review"
            outputObj.currentStep = _.findLast(outputObj.review_steps, (step) => {
                return step.complete;
            });
            var fullfilled = _.filter(outputObj.review_steps, (step) => {
                return step.complete;
            });
            outputObj.fullfilledCount = fullfilled.length;
            // check percent complete
            outputObj.percentDone = outputObj.fullfilledCount / outputObj.review_length
            // set status
            if(outputObj.fullfilledCount > 0) {
                outputObj.status = 'pending';
            }
            if(outputObj.fullfilledCount == outputObj.review_length) {
                outputObj.status = 'complete';
            }
            return outputObj;
    }
}

module.exports.step = step;
module.exports.run = run;
