var _ = require('lodash');
var fs = require('fs');
var Promise = require('bluebird');

function run(pipeline, settings) {
    var list = pipeline.getKey('concepts');
    return Promise.resolve(list)
        .then(_extract())
        .then(_truncate())
        .then(_sort())
        .then((sources) => {
            return Promise.resolve(settings.output(sources));
        });
}

function step(settings) {
    return {
        settings: settings || {},
        run: run
    }
}

function _extract() {
    return function(list) {
        return _.map(list, 'source');
    }
}

function _truncate() {
    return function(sources) {
        return _.map(_.union(sources), _.trim).filter(Boolean);
    }
}

function _sort() {
    return function(sources) {
        return _.sortBy(sources, (item) => item);
    }
}

module.exports.step = step;
module.exports.run = run;
