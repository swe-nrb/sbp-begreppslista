# SBP Begreppslista - Den öppna begreppslistan

Detta projekt är ett initiativ för att samla och harmonisera begrepp. Det finns inga begräsningsar för vilka begrepp som är lämpliga att lagra här men listan har ett naturligt fokus på samhällsbyggnad.

## Webbplats

http://nationella-riktlinjer.se/concepts

## Förvaltare

Du hittar aktiva förvaltare [här](https://gitlab.com/swe-nrb/sbp-begreppslista/project_members).

## Stadgar

Detta informationspaket förvaltas enligt nedanstående stadgar.

### Förvaltningsprocessen

Alla begrepp är välkomna, förvaltningsgruppen vädjar dock om att hålla listan "samhällsbyggnadsfokuserad". Det finns idag tre kravnivåer för begrepp, den första nivån är tekniska krav för att begreppet överhuvudtaget ska komma in i listan och de två andra nivåerna är kvalitetskrav med syfte att erbjuda konsumenterna ett så bra material som möjligt.

Den föregående nivån måste alltid vara uppfylld för att ett begrepp ska kunna gå vidare till nästa nivå.

**1. Tekniska krav**

1. Begreppet lagras som en fil enligt senast gällande [schema](http://schemas.nationella-riktlinjer.se/nrb-concept.2.schema.json) (kallat begreppsfil).
2. Begreppsfilen lagras i mappen `/raw_files` i detta informationspaket.
3. Begreppsfilen är en korrekt [JSON-fil](https://www.json.org/).

**2. Kvalitetsgranskad**

1. Fältet `term` skrivs med gemen bokstav i början och utan punkt (ex. objekt, attribut, modell).
    * Akronymer tillåts inte som term, skriv alltid ut termen och skriv in förkortningen i fältet `abbrevation`
    * Undantag görs för egennamn som skrivs med versal bokstav i början (ex. Lantmäteriet).
2. Fältet `term` skrivs i [singularis](https://sv.wikipedia.org/wiki/Singularis).
3. Fälten `definition` och `comment` skrivs som [fullständiga meningar](http://www.prefix.nu/fullstandig-mening.html).
4. Fältet `definition` ska ensamt kunna ge tillräcklig kontext för att definiera begreppet.
    * Fältet `comment` ska alltså inte behöva läsas för att förstå termens innebörd.
5. Fältet `comment` används för att ge läsaren värdefull indirekt information om begreppet.
    * Se begreppet *Digital terrängmodell* på [webbplatsen](http://www.nationella-riktlinjer.se/concepts?term=Digital%20terr%C3%A4ngmodell) för exempel.
6. Källan i fältet `source` ska vara relevant och allmänt känd
7. Ev. länkar ska vara fungerande och bör inte peka till "låsta" webbplatser
8. Begreppet får inte vara förvirrande gentemot redan kvalitetssäkrade begrepp
    * Om termen redan finns definierad ska den nya definition tydligt urskilja sig (vi vill inte ha flera begrepp som egentligen betyder samma sak). Notera att det dock är okej ur ett kvalitetsperspektiv att ha olika defitioner på en term (ex. våg på havet eller våg i håret).

**3. NRB Rekommendation**

NRB rekommendationen är ett sätt att tydligt markera vilka begrepp som är lämpliga att använda i tal och skrift inom samhällsbyggnad. Särskilt viktigt är det när det finns flera begrepp för samma term. Rekommendationerna bygger på subjektiva åsikter och det är därför viktigt att begreppen erhåller nivån via en transparent process och att motivieringar dokumenteras för förståelse så att så hög acceptans som möjligt kan nås.

1. Begrepp som är särkilt relevanta för samhällsbyggnadssektorn ska få en rekommendation.
    * Allmänna begrepp som t.ex. data är inte föremål för en NRB rekommendation (sådana begrepp kan dock vara värdefulla i listan ändå).
2. I det fall ett begrepp erhåller en rekommendation gentemot liknande begrepp (eller begrepp med samma term) ska en tydlig motivering dokumenteras.
3. En rekommenderat begrepp måste tillgodose behovet hos en majoritet av branschen.
    * t.ex. genom att en beredning gjorts med ett öppet remissförfarande

## Hur kan man lämna förbättringsförslag?

Vem som helst kan komma in och påverka innehållet i begreppslistan, Innehållet styrs huvudsakligen av två typer av filer, begreppsfiler och taggfiler.

### Begreppsfil  

Du kan lägga till dina egna begrepp genom att skapa en begreppsfil i mappen
`/raw_files/concepts`. För att föreslå begrepp måste du vara inloggad, om du inte har ett gitlab konto så kan du skapa det [här](https://gitlab.com/users/sign_in) (det är kostnadsfritt).

**Mall**  

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/concept_v1.yml) under mappen `/support_files/schemas`.

### Taggfil

Taggar används för att "dekorera" begreppsdefinitioner. Taggens innebörd definieras i en ["tagg-definition"](https://gitlab.com/swe-nrb/sbp-begreppslista/blob/6b48861419dec1c11f2f397ffb7da66a8a9807b6/support_files/schemas/nrb-tag-definition.1.schema.json) tillsammans med en process som varje tagg ska genomgå för att taggen ska kunna tillskrivas begreppet. En tagg kan därför ha följande status:

* **Kandidat** - Taggen har tilldelats ett begrepp utan att något steg i processen är påbörjat
* **Pågående** - Taggen är under behandling
* **Fastställd** - Taggen har passerat alla steg i sin process och kan därmed tillskrivas begreppet

Samtliga taggar som finns definierade i projektet går att finna i mappen `/raw_files`.

#### Definierade taggar

##### Avrådes

Denna tags används i första hand för begrepp som är missvisande eller innehåller felaktigheter.

**Exempel**

Vi har en tagg som heter "kvalitetsgranskad". Taggen anger att begreppet har genomgått en kvalitetssäkringsprocess (som är dokumenterad under avsnitt "förvaltningsprocessen" i detta dokument).

Taggens definition finns dokumenterad [här](https://gitlab.com/swe-nrb/sbp-begreppslista/blob/6b48861419dec1c11f2f397ffb7da66a8a9807b6/raw_files/nrb-tag-definition-quality.json).  
Själva taggen definieras enligt schemat [nrb-tag](https://gitlab.com/swe-nrb/sbp-begreppslista/blob/6b48861419dec1c11f2f397ffb7da66a8a9807b6/support_files/schemas/nrb-tag.1.schema.json).
Se exempel på färdig taggfil [här](https://gitlab.com/swe-nrb/sbp-begreppslista/blob/6b48861419dec1c11f2f397ffb7da66a8a9807b6/raw_files/nrb-tag-quality-attribut-coclass.json)


**Mall**

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/tag_v1.yml) under mappen `/support_files/schemas`.

## Namngivning av filer

Ange begreppet som filnamn med datorvänliga tecken:

* a-z
* 0-9
* \- (dash) | _ (underscore) | , (comma)

__Exempel__

anlaggningsmodell-trafikverket.yml

## Steg-för-steg guider  

### Ansök om medlemskap (behövs för att lämna förbättringsförslag)  

1. Logga in på Gitlab
2. Ansök om medlemskap genom att klicka på request access under [projektets sida](https://gitlab.com/swe-nrb/sbp-begreppslista)
3. Direkt du har blivit godkänd kan du följa stegen under bidra

### Lägg till begrepp genom att skapa egna filer

1. Kopiera mallen [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/support_files/schemas/concept_v1.yml)
2. Skapa en ny fil under mappen underlag [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/new/master/raw_files/concepts?file_name=filename.yml) (om du inte är inloggad eller har ansökt om medlemskap så kommer du till en felsida)
3. Benämna filen enligt ovan (__OBS!__ Ange benämningen utan åäö och med små bokstäver, avsluta med filändelsen `.yml`, t.ex. "anlaggningsmodell.yml". Filnamn ska vara exakt samma som fältet `filename` i mallfil)
4. Spara som en ny utvecklingsgren (skapa ny "branch")
5. Skapa en ny ändringsbegäran (skapa en "merge request" från din nya utvecklingsgren till "master")
6. Förslag bearbetas av begreppslistans förvaltningsgrupp
