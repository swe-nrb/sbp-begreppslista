const list = require('./output_files/concepts');
let _ = require('lodash');
let request = require('request-promise');
let Promise = require('bluebird');
let fs = require('fs');
let path = require('path');

let problems = []

let OUTPUT_PATH = path.resolve('problems.json');

Promise
    .resolve()
    .then(() => {
        return Promise.mapSeries(list, (item) => {
            if(!item.links) return null;
            return Promise.mapSeries(item.links, (link) => {
                console.log('Testing: ' + link.uri);
                return request
                    .get(link.uri)
                    .then((data, code) => {
                        //console.log(data, code);
                        console.log('OK');
                        return null;
                        return data;
                    })
                    .catch((err) => {
                        console.log('NOT OK', err.statusCode);
                        problems.push({
                            slug: item.slug,
                            gitlab_url: 'https://gitlab.com/swe-nrb/sbp-begreppslista/blob/master/raw_files/' + item.slug + '.json',
                            statusCode: err.statusCode,
                            faulty_url: link.uri
                        });
                    })
            })
        });
    })
    .then(() => {
        fs.writeFileSync(OUTPUT_PATH, JSON.stringify(problems), null, 2);
    });
