var _ = require('lodash');

var infopack = require('infopack');
var markupGenerator = require('sbp-markup-generator-official');

var jsonToXlsx = require('infopack-gen-json-to-xlsx');
var objectParser = require('infopack-gen-object-parser');
var createSource = require('./generators/create-source');
var tagGenerator = require('./generators/tag-generator');
var synonymPopulator = require('./generators/synonym-populator');

var pipeline = new infopack.Pipeline();

pipeline.addStep(objectParser.step({ GLOB_PATTERNS: ['raw_files/**', 'support_files/**'] }))

pipeline.addStep({ run: function(pipeline) {
	return Promise
		.resolve()
		.then(function() {
			pipeline.setKey('concepts', _.filter(pipeline.getKey('validObjects'), { _schema: "http://schemas.nationella-riktlinjer.se/nrb-concept.2.schema.json" }));
			return;
		});
}});

pipeline.addStep(createSource.step({
	output: (data) => {
		pipeline.writeFile({
			path: `${pipeline.getOutputDirPath()}/sources.json`,
			data: JSON.stringify(data, '', 2),
			title: 'Lista på alla källor'
		});
	}
}));

pipeline.addStep(tagGenerator.step({
	filename: 'collection'
}));

pipeline.addStep(synonymPopulator.step());

pipeline.addStep({ run: (pipeline) => {
	return Promise
		.resolve()
		.then(function() {
			pipeline.addLog(`${pipeline.getKey('invalidObjects').length} failed validation, se output/failed-objects.json for details`);
			pipeline.writeFile({
				path: `${pipeline.getOutputDirPath()}/invalid-objects.json`,
				data: JSON.stringify(pipeline.getKey('invalidObjects'), '', 2),
				title: 'Invalid objects'
			});
			pipeline.writeFile({
				path: `${pipeline.getOutputDirPath()}/valid-objects.json`,
				data: JSON.stringify(pipeline.getKey('validObjects'), '', 2),
				title: 'Valid objects'
			});
			return;
		})
}});

pipeline.addStep({ run: function(pipeline) {
	return Promise.resolve()
		.then(function() {
			pipeline.writeFile({
				path: `${pipeline.getOutputDirPath()}/concepts.json`,
				data: JSON.stringify(pipeline.getKey('concepts'), null, 2),
				title: 'Begreppslista',
				description: 'En lista i .json format innehållande samtliga begrepp'
			});
			return;
		});
}});

// generate excel file from json
pipeline.addStep(jsonToXlsx.step({
	storageKey: 'concepts',
	mapper: (conceptObj) => {
		return {
		    "Slug": _.get(conceptObj, 'slug'),
			"Term": _.get(conceptObj, 'term'),
			"Böjningar": _.join(_.get(conceptObj, 'inflections'),', '),
			"Källa": _.get(conceptObj, 'source'),
			"Språk": _.get(conceptObj, 'language'),
			"Definition": _.get(conceptObj, 'definition'),
			"Kommentar": _.get(conceptObj, 'comment'),
			"Contributor - namn": _.get(conceptObj, 'contributors[0].name'),
			"Contributor - LinkedIn": _.get(conceptObj, 'contributors[0].linkedin'),
			"Contributor - Email": _.get(conceptObj, 'contributors[0].email')
		};
	}
}));

// run pipeline
pipeline.run()
	.then(function(data) {
		// generate static page
		pipeline.generateStaticPage();
		console.log(pipeline.prettyTable());
	});
